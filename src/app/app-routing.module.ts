import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './common/components/dashboard/dashboard.component';
import { ProjectsComponent } from './projects/projects.component';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './common/components/layout/layout.component';
import { SidebarComponent } from './common/components/sidebar/sidebar.component';
const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },

  {
    path: 'dashboard',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
    ],
  },
  {
    path: 'projects',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: ProjectsComponent,
      },
    ],
  },
  {
    path: '**',
    component: LoginComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
export const routingComponents = [
  DashboardComponent,
  ProjectsComponent,
  LoginComponent,
  LayoutComponent,
  SidebarComponent,
];
