import { Component, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { SpaceService } from '../services/space.service';

/**
 * Food data with nested structure.
 * Each node has a name and an optional list of children.
 */
interface FoodNode {
  name: string;
  type: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: 'space1',
    type: 'space',
    children: [
      {
        name: 'Project 1',
        type: 'project',
        children: [{ name: 'Broccoli',type: 'list' }, { name: 'Brussels sprouts',type: 'list' }],
      },
      {
        name: 'Project 2',
        type: 'project',
        children: [{ name: 'Pumpkins',type: 'list' }, { name: 'Carrots',type: 'list' }],
      },
    ],
  },
  {
    name: 'space2',
    type: 'space',
    children: [
      {
        name: 'Project 3',
        type: 'project',
        children: [{ name: 'Broccoli',type: 'list' }, { name: 'Brussels sprouts',type: 'list' }],
      },
      {
        name: 'Project 4',
        type: 'project',
        children: [{ name: 'Pumpkins',type: 'list' }, { name: 'Carrots',type: 'list' }],
      },
    ],
  },
];

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
})
export class ProjectsComponent implements OnInit {
  treeControl = new NestedTreeControl<FoodNode>((node) => node.children);
  dataSource = new MatTreeNestedDataSource<FoodNode>();
  public data =[];

  constructor(
    private spaceserive : SpaceService
  ) {
    this.dataSource.data = TREE_DATA;
  }

  hasChild = (_: number, node: FoodNode) =>
    !!node.children && node.children.length > 0;

  ngOnInit(): void {
    this.getTreeData()
  }
  getTreeData() {
    this.spaceserive.getSpaceTree().subscribe(
      (sucess:any) => {
        this.dataSource.data = sucess.returnObject;
      },
      (error) => {
          
      }
    )
  }


}
