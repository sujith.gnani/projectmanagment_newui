import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpaceService {
  private baseUrl = "https://localhost:44388/api/";
  constructor(
    private http: HttpClient
  ) { }

  getSpaceTree(){
    return this.http.get(this.baseUrl + 'Spaces/GetSpaceTree?ModuleID=m1&UserGUID=079FE257-A2CB-45D1-AC46-35B79B6C4F00');
  }
}
