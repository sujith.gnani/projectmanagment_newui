import { Component, OnInit, VERSION } from '@angular/core';
//import { GlobalComponent } from '../../../common/components/global/global.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  constructor() {}
  ngVersion: string = VERSION.full;
  matVersion: string = '5.1.0';
  breakpoint: number;
  breakpointTwo: number;
  ngOnInit() {
    // for responsive
    this.breakpoint = window.innerWidth <= 411 ? 1 : 3;
    this.breakpointTwo = window.innerWidth <= 411 ? 1 : 3;
  }

  onResize(event) {
    debugger;
    //this.contentMargin = 70;
    this.breakpoint = event.target.innerWidth <= 411 ? 1 : 3;
    //  this.isMenuOpen = false;
  }
  onResizeTwo(event) {
    debugger;
    //   this.contentMargin = 70;
    //   this.isMenuOpen = false;
    this.breakpointTwo = event.target.innerWidth <= 411 ? 1 : 3;
  }
}
